# Maintainer: Jami Kettunen <jami.kettunen@protonmail.com>
# Stable Linux kernel with patches for MSM8998 devices
# Kernel config based on: allnoconfig, msm8998.config and pmos.config

_flavor="postmarketos-qcom-msm8998"
pkgname=linux-$_flavor
pkgver=5.15.0
pkgrel=1
_commit="d9ad5a19b76bbf05bc9e76efd58c09aeb4985b3a"
pkgdesc="Mainline Kernel fork for MSM8998 devices"
arch="aarch64"
_carch="arm64"
_config="config-$_flavor.$arch"
url="https://gitlab.com/msm8998-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-zram
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-anbox
"
makedepends="
	bison
	findutils
	flex
	linux-headers
	openssl-dev
	perl
	postmarketos-installkernel
	xz
"

# Source
_repository="W.I.P_sagit_mainline"
source="
	https://github.com/degdag/$_repository/archive/$_commit.tar.gz
	$_config
"
builddir="$srcdir/$_repository-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
f46dd47d798ea4938cac48ac7baf5981b062ae06b9241e95b8488a273a7bbd262717963dfd836375303871b23c534f946be3c2b4e3c2707a4eb15e31c44bf1d2  d9ad5a19b76bbf05bc9e76efd58c09aeb4985b3a.tar.gz
c6bac01533a964028a214a3effa97c978170d901d0b876d30e0786488d3a171df10f0fd63143f65ebc82c65e36d2e7f77ef17884e3cf41b13644c4178a583ba1  config-postmarketos-qcom-msm8998.aarch64
"
